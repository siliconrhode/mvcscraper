﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MVCScraper.Models.Selenium.Extractors
{
    public static class ListFinder

    {
        public static IWebElement LoadPortfolio(IWebDriver driver, string portfolioName)

        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(90));

            if (portfolioName == "Retirement")
            {
                driver.Navigate().GoToUrl("https://finance.yahoo.com/portfolio/p_0/view/v1");

                try
                {
                    // kill popup
                    wait.Until(drv => drv.FindElement(By.XPath("//button[@title = 'Close']")));
                    IWebElement closePopup = driver.FindElement(By.XPath("//button[@title = 'Close']"));
                    closePopup.Click();

                    wait.Until(drv => drv.FindElement(By.Id("main")));
                    Console.WriteLine("Retirement Portfolio loaded successfully");

                }
                catch
                {
                    Console.WriteLine("List finder: Failed to load Retirement Portfolio");
                }

                Console.WriteLine();
                

            }

            IWebElement portfolio = driver.FindElement(By.Id("main"));
            return portfolio;

        }

    }
}
