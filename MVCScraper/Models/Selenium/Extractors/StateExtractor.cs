﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace MVCScraper.Models.Selenium.Extractors
{
    public static class StateExtractor
    {
        public static List<StockState> Extract(IWebElement stateData, IWebDriver driver)
        {
            Console.WriteLine("Extracting Data....");
            var stocksStateList = new List<StockState>();
            var unprocessedRaws = stateData.FindElements(By.XPath("//tr"));
            
            for (int i =1, j = unprocessedRaws.Count; i <= j-2; i++)
            {
                var stockState = new StockState();

                stockState.Symbol = stateData.FindElement(By.XPath($"//tbody[1]/tr[{i}]/td/span/a")).Text;
                stockState.CurrentPrice = double.Parse(stateData.FindElement(By.XPath($"//tbody/tr[{i}]/td[2]/span")).Text);
                stockState.PriceChangeDollars =
                double.Parse(stateData.FindElement(By.XPath($"//tbody[1]/tr[{i}]/td[3]/span")).Text);
                stockState.PriceChangePercent =
                    double.Parse(stateData.FindElement(By.XPath($"//tbody[1]/tr[{i}]/td[4]/span[1]")).Text.Trim('%'));
                stocksStateList.Add(stockState);
            }
            return stocksStateList;
            
        }
    }
}