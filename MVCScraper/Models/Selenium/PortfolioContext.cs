﻿using System.Data.Entity;

namespace MVCScraper.Models.Selenium
{
    public class PortfolioContext : DbContext
    {
        public PortfolioContext()
            : base("PortfolioDatabase")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<PortfolioState> PortfolioState { get; set; }
        //public DbSet<StockState> StockState { get; set; }
    }
}