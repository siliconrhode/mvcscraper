﻿using System;
using System.Linq;
using System.Web.Mvc;
using MVCScraper.Models.Selenium;
using MVCScraper.Models.Selenium.Extractors;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Data.Entity;
using Newtonsoft.Json;

namespace MVCScraper.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult ViewSnapshots()
        {
            using (var db = new PortfolioContext())
            {
                return View(db.PortfolioState.OrderBy(x => x.TimeOfExtraction).ToList());
            }
        }

        [Authorize]
        public ActionResult RunScraper()
        {
            using (var db = new PortfolioContext())
            {
                // Instantiate ChromeDriver and sign in to Yahoo finance
                IWebDriver driver = new ChromeDriver();
                Authenticator.SignIn(driver);
                IWebElement portfolio = ListFinder.LoadPortfolio(driver, "Retirement");
                var portfolioState = PortfolioExtractor.Extract(portfolio, driver);
                //DislplayOnConsole.Display(portfolioState);

                db.PortfolioState.Add(portfolioState);
                db.SaveChanges();
                Console.WriteLine("Sucessfully Saved Portfolio Data");
                return RedirectToAction("ViewSnapshots");
            }
        }

        [Authorize]
        public ActionResult ViewSnapshot(int id)
        {
            using (var db = new PortfolioContext())
            {
                var snapshot = db.PortfolioState.Include(x => x.StocksState).FirstOrDefault(x => x.Id == id);
                return View(snapshot);
            }
        }

        [Authorize]
        public ActionResult ViewChart()
        {
            using (var db = new PortfolioContext())
            {
                var data = db.PortfolioState.OrderBy(x => x.TimeOfExtraction).ToList();
                ViewBag.DataPoints = JsonConvert.SerializeObject(data);
                return View();
            }
        }

    }
}