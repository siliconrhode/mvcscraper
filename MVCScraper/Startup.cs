﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MVCScraper.Startup))]
namespace MVCScraper
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
